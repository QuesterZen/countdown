# Countdown

## Description

Solves problems of the form:
"Using the numbers 

`100, 75, 25, 9, 8, 3` 

exactly once each,
combining them using the basic arithmetic operators 

`+, -, *, /`,

produce an expression that evaluates to 

`994`"

## Forumations of arithmetic expressions

The first problem is how to represent an arithmetic expression.
One way is to use normal arithmetic with normal operator precidence.

A solution would be something like:

`(75 - 100 / 25) * (9 + 8 - 3)`

Adding brackets to make precidence explicit removes ambiguity and makes it
simpler for computers to parse. The expression becomes:

`(75 - (100 / 25)) * ((9 + 8) - 3)`

A better solution is to represent the expressions as a binary tree:

```
            75
         /
      -          100
   /     \    /
  /         / 
*             \
  \              25
   \
    \                9
     \             /
      \         +
       \     /     \
          -           8
             \
                3
```

This can be equivalently (and without precidence or bracketing issues) be represented
in prefix (LISP) format as

`(* (- 75 (/ 100 25)) (- (+ 9 8) 3)))`

or postfix (FORTH) format as

`75 100 25 / - 9 8 + 3 - *`

I chose the prefix form for simplicity in production, representation and evaluation,
particularly as this yields valid Scheme expressions.

The ability to establish a one-to-one correspondance between binary trees and expressions
in each of the other forms provides a simple proof that all four forms are equivalent.

## How many different expressions are there?

To count the number of possible sums, the postfix form is the easiest
representation to think with.
Since postfix uses the stack, the constraints for correct formulation of 
expressions are:

1) We have one fewer operators than numbers 
(each numbers adds one to the stack, each operator reduces the values by one,
and we need to end up with a single result on the stack)

2) All expression prefixes contain at least one more number than operators 
(we can never use an operator when there are fewer than two values on the stack)

Working backwards, the final step must be an operator and two values on the stack.
Each of these values must be generated from valid expressions with fewer values.
A single value is a valid expression.
This gives us a simple recurrence relationship for the number of expressions
we can generate

`C(n) = Sum[i=1 to n-1] C(i) . C(n-i)`

`C(1) = 1`

These give the sequence: 

`1 1 2 5 14 42 ...`

These are the Catalan numbers, which crop up so often in combinatorics they are often
included in text books alongside the more familiar Factorial numbers. A trip to
the Online Encyclopedia of Integer Sequences tells us reassuringly that these numbers
variously represent:
1) the number of ways to insert n bracket pairs into a string of n+1 letters
2) the number of ways to arrange +1 and -1 steps so the total is never negative
3) and the number of full binary trees with n leaves;

ie they represent the number of ways to generate expressions
using arithmetic, postfix or prefix, and tree forms respectively.

It is easy to verify the explicit formula in OEIS for the Catalan numbers.

`C(n) = Binominal(2(n-1), (n-1)) / n`

So in our example,

`C(6) = 42`

(For more on the Catalan numbers see:
* OEIS: https://oeis.org/A000108
* Wikipedia: https://en.wikipedia.org/wiki/Catalan_number
* MathWorld: http://mathworld.wolfram.com/CatalanNumber.html)

To determine how the number of values and operations affects the count,
it is easier to switch to thinking in terms of the binary tree representation.

We can order the numeric values in `n!` ways
(the n permutations of terminal leaves on any given binary tree).

We can also provide `4^(n-1)` different operator combinations
(ie. any of the 4 operators can be placed at any of the (n-1) nodes
of any given binary tree).

So in total there are

`(2n-2)! / (n-1)! * 4^(n-1)`

unique sums.

For the example aboive this gives a total of `30,965,760` sums to evaluate,
which is large, but well within reach of modern computers.

## Symmetries and Equivalence in Solutions

In reality it is not so simple.
`8 3 + 9 +` and `9 8 3 + +` are not really different solutions.
Addition is both commutative and associative, so with only three numbers
there are 12 different ways to write `3 + 8 + 9`. 
As a result, there is going to be a lot of duplication in the expressions we
search.

It gets still worse. There are more subtle symmetries to consider.

Are `3 8 - 25 75 - *` and `8 3 - 75 25 - *` different solutions?

What about `3 8 9 / /` and `9 3 * 8 /`?

Although we consider them conceptually the same, the computer calculations and
the transformation rules from one to the other are more complicated.

For this exercise I chose to count ALL solutions, not 'equivalence classes' of
symmetrically similar solutions. For the example above there are 192 'solutions'
but they can be grouped into just 5 'unique concepts'. I started to implement an
equivalence operator to judge whether two solutions are simple transformations
of each other and so 'equivalent', but decided
not to pursue it further than the concept stage.

On the other hand if we could avoid generating symmetrically equivalent expressions
we could cut the search space by an order of magnitude 
(see Performance Considerations below).

## Memory Considerations

Generating all 31 million sums is feasible on a large computer if an efficient
representation is made. In a language like C, we could store each sum in an 8 byte
array, requiring approximately 256MB. However, using a list structure and Scheme
objects, it is likely that each sum will require something like 512 bytes, requiring
almost 15GB of memory. Over 1GB, paging becomes a serious performance concern.

Therefore it would be more effective to generate expressions as needed. The ideal
approach is to use co-routines or generators to generate the sums on the fly.

One approach is to breakdown the problem in the same way we enumerated the
number of possible expressions:
- a generator of binary tree structures
- a generator of operator combinations to populate the nodes of the trees
- a generator of permutations of values to populate the leaves of the trees

The cartesian product of these three generators would allow us to produce all
possible expressions.

Since Scheme lacks generators, there are a few alternatives. 
1) streams using delay and force
2) use nested iterative loops and maintain state for each generator
3) implement Python's generators in Scheme!

I chose to implement generators, because the ability to combine generators makes
for far neater code. Scheme's macros and continuations make it a relatively
simple exercise. A simplified version shows how easy it is!

```
(define-syntax (make-generator stx)
  (syntax-case stx ()
    [(_ . body)
     #`(let ((resume #f)
             (break #f))
         (define (#,(datum->syntax stx 'yield) v)
           (set-contunation!
            resume
            (break v)))
         (λ ()
           (if resume
               (resume '())
               (set-continuation!
                 break
                 (begin . body)
                 'done))))]))
```

In addition, I have added conveniences for iterating over generators (`for-generator`),
combining generators (`cartesian-product-gen` and `sequential-gen`)
and ways to convert to/from lists (`generator->list` and `list->generator`) to
create a genuinely useful re-usable abstraction.

Now we can express our enumeration of expressions extremely elegantly.

```
(define (expression-generator values ops)
  (make-generator
    (for-generator
      (cartesian-product-generator
        (tree-pattern-generator (length values))
        (product-generator ops (sub1 (length values)))
        (permutation-generator values))
      (λ (elements)
        (yield (apply create-tree elements))))))
```

In actuality I don't do things this way as this approach using pattern substitution 
is a bit too slow, although the first version of the program used exactly this
code.
Instead I use a method that recursively partitions the values and uses them to
generate left and right subtrees independently
(see Performance Considerations).

## Performance Considerations

Finding solutions is rather slow and initial versions of the program took several
hours to run.
There are three main drivers of performance we can address 
to achieve performance gains:

1) The number of sums to check

    The issue of symmetries discussed above may provide a way to reduce the
    search space substantially. The simplest would be to eliminate commutative
    equivalents in multiplication and addition (for example we skip all
    expressions `a + b` or `a * b` in which `a > b`).
    
    A 2x improvement in performance appears relatively easy to obtain. Further
    gains may also be possible with the elimination of other symmetries during
    the enumeration phase, but may be harder to identify.
    
    An alternative approach is to alter the rules of the game. If we restrict
    expressions so that the intermediates are positive integers for example,
    we can artificially reduce the search space to something more manageable.
    
    If we do not wish to be exhaustive, and only search for some of the solutions,
    we can use heuristics to improve the chances of finding a solution quickly.
    For example, the current version looks at very unbalanced binary trees first,
    giving expressions of the form:
    
    `v v v v v v o o o o o` (v is a value, o is an operator)
    
    which are less likely to produce a solution than the more balanced:
    
    `v v o v o v v o v o o`.
    
    Also, solutions are more likely to contain addition than division. Other
    heuristics are that we are more likely to multiply or divide by small numbers
    and add or subtract large ones. Using these heuristic can greatly improve the
    speed of finding a first solution (or first few solutions) and of finding
    more 'human-friendly' solutions.
    
    Another version of the game allows the player to use only some of the values.
    When there is such a solution, the exponential nature of the task means that
    it will be found much more quickly than solutions with more values.

2) The time required to enumerate the sums

    The initial version used pattern-substitution to inject values and operators
    into binary tree structures. A substantial performance gain came from
    switching to generating the entire tree recusively, using a partition
    generator to divide the values between left and right subtrees.
    
    Further efficiencies might be possible by eliminating the use of recursive
    generators: in particular by generating the list of partitions in advance
    and looping through them sequentially. A 25-50% speed up is plausible.

3) The speed of evaluating the sums

    The original formulation used postfix expressions, which required a stack 
    (either an explicit stack implementation or an implicit version using
    nested function calls).
    
    Given that we are working in Scheme, it is clearly prefereable to use
    postfix S-expressions so that the expressions we generate are runnable
    Scheme expressions. It turned out that using `eval` was slower than writing
    a custom evaluator.
    
    It is hard to imagine substantial further improvements in Scheme.

The current program gives acceptable (faster than human) performance for 6-value 
problems, but larger problems are likely to require a significant re-think.
It may require reimplementation of large parts of the program in C to achieve
acceptable performance for 8 or 9 value problems. Beyond that, the combinatorial
nature of the problem means that exhaustive search is no longer likely to be a
reasonable approach.

After a couple of rounds of performance improvement I was able to achieve a 30x 
improvement in running time, but have reached a point of diminishing returns
and lack the motivation to pursue further gains. 

```
Current Performance Benchmarks
(2.3 GHz Intel Core i5, 2 core MacBook Pro laptop)
(average of 3 runs)

Problem: values: '(100 75 25 9 8 3), operations: '(+ - * /), target: 994
First solution: 42.1s
All solutions: 5min 42.6s
```

## Code

Written in Racket Scheme version 7.1

`generator.rkt`
    Library for using Python-like generators in Racket Scheme
    
`generators.rkt`
    Some useful generic generators such a permutations and partitions
    
`countdown.rkt`
    Problem solver implemented as a generator.
    
## Licence

Please feel free to make use of the generator library.
The code is available for use under a GPL3 licence.

## Contributions

If you find the code useful, you are kindly requested to let me know if any
improvements, suggestions or problems you have so that I can continue to improve
the code for other users.
